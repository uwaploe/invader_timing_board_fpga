`timescale 1ns / 1ps

module proc
  (
   input 	clk,
   input 	rst,
   input 	enable,
   input [31:0] trig_dly,
   input [15:0] dio_on,
   input [31:0] qs_dly,
   input [15:0] qs_on,
   input [31:0] extt_dly,
   input [15:0] extt_on,
   input [31:0] extg_dly,
   input [15:0] extg_on,
   output 	dio_trig,
   output 	qs_trig,
   output 	ext_trig,
   output 	ext_gate
   );

   reg [31:0] 	trig_dly_l;
   reg [31:0] 	trig_cnt;
   reg 		trig;
   reg 		rst_t1;

   always @(posedge clk) begin
      rst_t1 <= rst;

      if (!rst && rst_t1)
	trig_dly_l <= trig_dly;
      else
	trig_dly_l <= trig_dly_l;

      if (~|trig_cnt && !rst && enable)
	trig <= 1;
      else
	trig <= 0;
      
      if (!rst && rst_t1)
	trig_cnt <= 0;
      else
	if (trig_cnt < trig_dly_l)
	  trig_cnt <= trig_cnt + 'b1;
	else
	  trig_cnt <= 0;
   end

   pon pon_extdiode_inst
     (
      .clk(clk),
      .rst(rst),
      .trig(trig),
      .delay(32'd0),
      .uptime(dio_on),
      .dout(dio_trig)
      );

   pon pon_qswitch_inst
     (
      .clk(clk),
      .rst(rst),
      .trig(trig),
      .delay(qs_dly),
      .uptime(qs_on),
      .dout(qs_trig)
      );

   pon pon_exttrigger_inst
     (
      .clk(clk),
      .rst(rst),
      .trig(trig),
      .delay(extt_dly),
      .uptime(extt_on),
      .dout(ext_trig)
      );

   pon pon_extgate_inst
     (
      .clk(clk),
      .rst(rst),
      .trig(trig),
      .delay(extg_dly),
      .uptime(extg_on),
      .dout(ext_gate)
      );

endmodule
      
