
`timescale 1 ns / 1 ps

module tb_pon;

   reg clk;
   reg rst;
   reg trig;
   reg [31:0] delay;
   reg [15:0] uptime;
   wire       dout;
   integer i;
	
   pon pon_inst
     (
      .clk(clk),
      .rst(rst),
      .trig(trig),
      .delay(delay),
      .uptime(uptime),
      .dout(dout)
      );

   initial begin
      clk = 1;
      forever #1 clk = !clk;
   end

   initial begin
      rst = 0;
      trig = 0;
      delay = 0;
      uptime = 0;
      #100;
      rst = 1;
      #2;
      rst = 0;
      #98;

		for (i = 0; i < 4; i = i + 1) begin
			delay = i;
			uptime = 8;
			trig = 1;
			#2;
			trig = 0;
			#98;
		end
		
		delay = 0;
		uptime = 16'hFFFF;
		trig = 1;
		#2;
		trig = 0;
		#98;
		
		delay = 0;
		uptime = 0;
		trig = 1;
		#2;
		trig = 0;
		#98;
		
		delay = 0;
		uptime = 1;
		trig = 1;
		#2;
		trig = 0;
		#98;
		
		delay = 32'hFFFFFFFF;
		uptime = 2;
		trig = 1;
		#2;
		trig = 0;
		#98;
				
   end
   
endmodule

   
