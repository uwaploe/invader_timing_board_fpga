`timescale 1ns / 1ps

//`define NOIFACE

`ifdef NOIFACE  // @ 200 MHz
 `define DFREQ     32'd10000000 // 50ms
 `define EDON      16'd6000     // 30us
 `define QSDLY     32'd40000    // 200us
 `define QSON      16'd6000     // 30us
 `define QSETDLY   32'd39980    // 200us-100ns
 `define ETON      16'd6000     // 30us
 `define QSETEGDLY 32'd39980    // 200us-100ns
 `define EGON      16'd10       // 50ns
`endif

module main
  (
   input  CLK,
   output PHY_RST_N,
  
   input  FPGA_EN,
   
   output DIO_TRIG,
   output QS_TRIG,
   output EXT_TRIG,
   output EXT_GATE,

   input  FPGA_MUX0, 
   input  FPGA_MUX1, 
   input  FPGA_MUX2, 
   input  FPGA_MUX3  
   );

   wire   clk;
   wire   sclk;
   wire [23:0] spi_data;
   wire        spi_valid;
   wire        srun;
   wire [31:0] strig_dly;
   wire [15:0] sdio_on;
   wire [31:0] sqs_dly;
   wire [15:0] sqs_on;
   wire [31:0] sextt_dly;
   wire [15:0] sextt_on;
   wire [31:0] sextg_dly;
   wire [15:0] sextg_on;

`ifdef NOIFACE
   reg [17:0]  dbg_rst_cntr = 0;
   reg 	       dbg_rst;
   
   always @(posedge clk) begin
      if (&dbg_rst_cntr)
	dbg_rst_cntr <= dbg_rst_cntr;
      else
	dbg_rst_cntr <= dbg_rst_cntr + 'b1;

      if (&dbg_rst_cntr)
	dbg_rst <= 0;
      else
	dbg_rst <= 1;
   end
   
   proc proc_inst
     (
      .clk(clk),
      .rst(dbg_rst),
      .enable(FPGA_EN),
      .trig_dly(`DFREQ),
      .dio_on(`EDON),
      .qs_dly(`QSDLY),
      .qs_on(`QSON),
      .extt_dly(`QSETDLY),
      .extt_on(`ETON),
      .extg_dly(`QSETEGDLY),
      .extg_on(`EGON),
      .dio_trig(DIO_TRIG),
      .qs_trig(QS_TRIG),
      .ext_trig(EXT_TRIG),
      .ext_gate(EXT_GATE)
      );
`else      
   proc proc_inst
     (
      .clk(clk),
      .rst(!srun),
      .enable(FPGA_EN),
      .trig_dly(strig_dly),
      .dio_on(sdio_on),
      .qs_dly(sqs_dly),
      .qs_on(sqs_on),
      .extt_dly(sextt_dly),
      .extt_on(sextt_on),
      .extg_dly(sextg_dly),
      .extg_on(sextg_on),
      .dio_trig(DIO_TRIG),
      .qs_trig(QS_TRIG),
      .ext_trig(EXT_TRIG),
      .ext_gate(EXT_GATE)
      );
   
   control control_inst
     (
      .clk(sclk),
      .spi_valid(spi_valid),
      .spi_data(spi_data),
      .run(srun),
      .trig_dly(strig_dly),
      .dio_on(sdio_on),
      .qs_dly(sqs_dly),
      .qs_on(sqs_on),
      .extt_dly(sextt_dly),
      .extt_on(sextt_on),
      .extg_dly(sextg_dly),
      .extg_on(sextg_on)
      );
      
   spi_slave 
     #(.N(24))
   spi_slave_inst
     (
      .clk_i(sclk),
      .spi_ssel_i(FPGA_MUX1),
      .spi_sck_i(FPGA_MUX0),
      .spi_mosi_i(FPGA_MUX2),
      .spi_miso_o(),
      .di_req_o(),
      .di_i(24'd0),
      .wren_i(1'b0),
      .wr_ack_o(),
      .do_valid_o(spi_valid),
      .do_o(spi_data)
      );
`endif 

   clocking clocking_inst
     (
      .CLK_IN1(CLK),
      .CLK_OUT1(clk),
      .CLK_OUT2(sclk),
      .LOCKED(PHY_RST_N)
      );
      
endmodule
