
module control
  (
   input 	    clk,
   input 	    spi_valid,
   input [23:0]     spi_data,
   output reg 	    run,
   output reg [31:0] trig_dly,
   output reg [15:0] dio_on,
   output reg [31:0] qs_dly,
   output reg [15:0] qs_on,
   output reg [31:0] extt_dly,
   output reg [15:0] extt_on,
   output reg [31:0] extg_dly,
   output reg [15:0] extg_on 
   );

   initial run = 0;
      
   always @(posedge clk)
     if (spi_valid) begin
	case (spi_data[23:16])
	  8'h00 : begin
	     run <= spi_data[0];
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h10 : begin
	     run <= 1'b0;
	     trig_dly <= {spi_data[15:0], trig_dly[15:0]};
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end 
	  8'h11 : begin
	     run <= 1'b0;
	     trig_dly <= {trig_dly[31:16], spi_data[15:0]};
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h12 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= spi_data[15:0];
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h13 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= {spi_data[15:0], qs_dly[15:0]};
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h14 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= {qs_dly[31:16], spi_data[15:0]};
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h15 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= spi_data[15:0];
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h16 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= {spi_data[15:0], extt_dly[15:0]};
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h17 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= {extt_dly[31:16], spi_data[15:0]};
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h18 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= spi_data[15:0];
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	  8'h19 : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= {spi_data[15:0], extg_dly[15:0]};
	     extg_on <= extg_on;
	  end
	  8'h1A : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= {extg_dly[31:16], spi_data[15:0]};
	     extg_on <= extg_on;
	  end
	  8'h1B : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= spi_data[15:0];
	  end
	  default : begin
	     run <= 1'b0;
	     trig_dly <= trig_dly;
	     dio_on <= dio_on;
	     qs_dly <= qs_dly;
	     qs_on <= qs_on;
	     extt_dly <= extt_dly;
	     extt_on <= extt_on;
	     extg_dly <= extg_dly;
	     extg_on <= extg_on;
	  end
	endcase
     end
     else begin
	run <= run;
	trig_dly <= trig_dly;
	dio_on <= dio_on;
	qs_dly <= qs_dly;
	qs_on <= qs_on;
	extt_dly <= extt_dly;
	extt_on <= extt_on;
	extg_dly <= extg_dly;
	extg_on <= extg_on;
     end 
	  
endmodule
