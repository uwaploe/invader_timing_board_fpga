
`timescale 1 ns / 1 ps

module pon #(parameter DLY_WIDTH = 32,
	     parameter UPT_WIDTH = 16)
  (
   input 		 clk,
   input 		 rst,
   input 		 trig,
   input [DLY_WIDTH-1:0] delay,
   input [UPT_WIDTH-1:0] uptime,
   output reg 		 dout
   );

   reg [DLY_WIDTH-1:0] 	 delay_l;
   reg [UPT_WIDTH-1:0] 	 uptime_l;
   reg [DLY_WIDTH-1:0] 	 delay_cnt;
   reg [UPT_WIDTH-1:0] 	 uptime_cnt;
   reg 			 rst_t1;
   reg 			 trig_t1;
   reg 			 trig_t2;
   reg 			 running;
   reg 			 dout_m1;
   
   always @(posedge clk) begin
      rst_t1 <= rst;
      trig_t1 <= trig;
      trig_t2 <= trig_t1;
      dout <= dout_m1;
   end

   always @(posedge clk) 
     if (trig_t1 && !trig_t2) begin
	delay_l <= delay;
	uptime_l <= uptime;
     end
     else begin
	delay_l <= delay_l;
	uptime_l <= uptime_l;
     end
   
   always @(posedge clk)
     if (trig_t1 && !trig_t2)
       delay_cnt <= 0;
     else if (&delay_cnt || rst_t1)
       delay_cnt <= {DLY_WIDTH{1'b1}};
     else
       delay_cnt <= delay_cnt + 'b1;
   
   always @(posedge clk)
     if (running && delay_cnt == delay_l)
       uptime_cnt <= 0;
     else if (&uptime_cnt || rst_t1)
       uptime_cnt <= {UPT_WIDTH{1'b1}};
     else
       uptime_cnt <= uptime_cnt + 'b1;
	  
   always @(posedge clk)
     if (rst || (running && uptime_cnt == uptime_l))
       running <= 0;
     else if (trig_t1 && !trig_t2)
       running <= 1;
     else
       running <= running;
   
   always @(posedge clk)
     if (running && uptime_cnt < uptime_l)
       dout_m1 <= 1;
     else
       dout_m1 <= 0;
   
endmodule
   
