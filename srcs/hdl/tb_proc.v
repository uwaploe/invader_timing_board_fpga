`timescale 1ns / 1ps

`define TRIG_DLY 32'd16
`define DIO_ON   16'd4
`define QS_DLY   32'd4
`define QS_ON    16'd4
`define EXTT_DLY 32'd8
`define EXTT_ON  16'd4
`define EXTG_DLY 32'd12
`define EXTG_ON  16'd1

module tb_proc;

   reg clk;
   reg rst;
   reg enable;
   wire dio_trig;
   wire qs_trig;
   wire ext_trig;
   wire ext_gate;

   proc proc_inst
     (
      .clk(clk),
      .rst(rst),
      .enable(enable),
      .trig_dly(`TRIG_DLY),
      .dio_on(`DIO_ON),
      .qs_dly(`QS_DLY),
      .qs_on(`QS_ON),
      .extt_dly(`EXTT_DLY),
      .extt_on(`EXTT_ON),
      .extg_dly(`EXTG_DLY),
      .extg_on(`EXTG_ON),
      .dio_trig(dio_trig),
      .qs_trig(qs_trig),
      .ext_trig(ext_trig),
      .ext_gate(ext_gate)
      );

   initial begin
      clk = 1;
      forever #4 clk = !clk;
   end

   initial begin
      rst = 1;
      enable = 1;

      #400;
      rst = 0;
		
		#500;
		enable = 0;
   end

endmodule

   
